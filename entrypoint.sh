#!/bin/sh

# Global variables
DIR_CONFIG="/etc/v2ray"
DIR_RUNTIME="/usr/bin"
DIR_TMP="$(mktemp -d)"

uuid0=ea05aca2-b9cd-4fb5-82d6-78c67237b592
mypath0=/fdaFDvdaf3fdsa
myport0=10250

uuid1=40fc19be-d2ab-48cb-80b6-1670907c4b79
mypath1=/fdsa789JKDlkdfsa
myport1=10251

uuid2=73dd6b77-a967-4025-947d-e7dde4c2b346
mypath2=/fdsaKFDSklfdsa9
myport2=10252

uuid3=c389be81-f685-4e77-8368-52c3e6d30d36
mypath3=/fdsajkl98fdas
myport3=10253

uuid4=db72275d-8417-479c-b299-1246cd0615da
mypath4=/fas9fdsalKJd
myport4=10254


# Write V2Ray configuration
cat << EOF > ${DIR_TMP}/myconfig.pb
{
	"inbounds": [
		{
			"port": $myport0,
			"protocol": "vless",
			"settings": {
				"clients": [
					{
						"id": "$uuid0"
					}
				],
			"decryption": "none"
		},
		"streamSettings": {
			"network": "ws",
			"wsSettings": {
					"path": "$mypath0"
				}
			}
		},{
			"port": $myport1,
			"protocol": "vless",
			"settings": {
				"clients": [
					{
						"id": "$uuid1"
					}
				],
			"decryption": "none"
		},
		"streamSettings": {
			"network": "ws",
			"wsSettings": {
					"path": "$mypath1"
				}
			}
		},{
			"port": $myport2,
			"protocol": "vless",
			"settings": {
				"clients": [
					{
						"id": "$uuid2"
					}
				],
			"decryption": "none"
		},
		"streamSettings": {
			"network": "ws",
			"wsSettings": {
					"path": "$mypath2"
				}
			}
		},{
			"port": $myport3,
			"protocol": "vless",
			"settings": {
				"clients": [
					{
						"id": "$uuid3"
					}
				],
			"decryption": "none"
		},
		"streamSettings": {
			"network": "ws",
			"wsSettings": {
					"path": "$mypath3"
				}
			}
		},{
			"port": $myport4,
			"protocol": "vless",
			"settings": {
				"clients": [
					{
						"id": "$uuid4"
					}
				],
			"decryption": "none"
		},
		"streamSettings": {
			"network": "ws",
			"wsSettings": {
					"path": "$mypath4"
				}
			}
		}
	],
	"outbounds": [
		{
			"protocol": "freedom"
		}
	]
}
EOF

# Get V2Ray executable release
curl --retry 10 --retry-max-time 60 -H "Cache-Control: no-cache" -fsSL github.com/v2fly/v2ray-core/releases/latest/download/v2ray-linux-64.zip -o ${DIR_TMP}/v2ray_dist.zip
unzip ${DIR_TMP}/v2ray_dist.zip -d ${DIR_TMP}

# Convert to protobuf format configuration
mkdir -p ${DIR_CONFIG}
mv -f ${DIR_TMP}/myconfig.pb ${DIR_CONFIG}/myconfig.json

# Install V2Ray
install -m 755 ${DIR_TMP}/v2ray ${DIR_RUNTIME}
rm -rf ${DIR_TMP}

# Run V2Ray
supervisord -c /etc/supervisor/conf.d/supervisord.conf